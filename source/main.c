#include <stdio.h>
#include "dice.h"

int main(){             //implements a number of faces of a dice
    initializeSeed();
    int faces;
    printf("How many face will your dice have?\n");
    scanf("%d",&faces);
    printf("Let's roll the dice : %d\n", rollDice(faces));
    return 0;
}

